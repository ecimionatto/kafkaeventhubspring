package com.example.demo;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.support.TestPropertySourceUtils;

import static com.example.demo.KafkaTestContainersConfiguration.KAFKA_CONTAINER;

public class PropertyContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        KAFKA_CONTAINER.start();
        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                configurableApplicationContext, "spring.kafka.bootstrap-servers=" + KAFKA_CONTAINER.getBootstrapServers());

    }
}
