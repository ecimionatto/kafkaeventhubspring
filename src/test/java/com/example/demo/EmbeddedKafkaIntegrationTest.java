package com.example.demo;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest (properties = "spring.kafka.bootstrap-servers=localhost:9093")
@DirtiesContext
@EmbeddedKafka(partitions = 1, brokerProperties = {"listeners=PLAINTEXT://localhost:9093", "port=9093", "socket.request.max.bytes=500000000"})
class EmbeddedKafkaIntegrationTest {

    @Autowired
    private ConsumerService consumer;

    @Autowired
    private ProducerService producer;

    @Test
    public void givenEmbeddedKafkaBroker_whenSendingtoSimpleProducer_thenMessageReceived() throws InterruptedException {
        String message = UUID.randomUUID().toString();
        producer.send(message);
        TimeUnit.SECONDS.sleep(1);
        assertThat(consumer.getLatest(), CoreMatchers.hasItems(message));
    }
}
