package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ActiveProfiles("integration-test")
@SpringBootTest()
class DemoApplicationTests {

    @Autowired
    ConsumerService consumer;

    @Test
    void shouldSendAndReceiveMessage() throws InterruptedException {
        final String message = UUID.randomUUID().toString();
        TimeUnit.SECONDS.sleep(5);
        System.out.println("latest..." + consumer.getLatest());
        assertTrue(consumer.getLatest().size() > 0);
    }

}
