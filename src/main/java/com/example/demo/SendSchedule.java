package com.example.demo;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class SendSchedule {

    private static Logger LOG = LoggerFactory.getLogger(SendSchedule.class);

    final ProducerService producerService;

    public SendSchedule(ProducerService producerService) {
        this.producerService = producerService;
    };

    @Scheduled(fixedRate = 1000)
    public void send() {
        LOG.debug("sending...");
        producerService.send(UUID.randomUUID().toString());
    }

}
