package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Component
public class ProducerService {

    private static Logger LOG = LoggerFactory.getLogger(ProducerService.class);

    @Autowired
    private final KafkaTemplate kafkaTemplate;

    private static final String TOPIC = "messages";

    @Autowired
    public ProducerService(KafkaTemplate kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void send(final String message) {
        try {
            ListenableFuture future = this.kafkaTemplate.send(TOPIC, UUID.randomUUID().toString(),
                    message);
            LOG.debug("sending...", future.get().toString());
        } catch (InterruptedException | ExecutionException e) {
            throw new IllegalStateException(e);
        }

    }

}
