package com.example.demo;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ConsumerService {

    final private Logger LOG = LoggerFactory.getLogger(ConsumerService.class);

    List<String> messages = new ArrayList<>();

    @KafkaListener(topics = "messages")
    public void receive(final String message) {
        LOG.info("receiving...{}", message);
        messages.add(message);
    }

    public List<String> getLatest() {
        return messages;
    }
}
